'use strict';
/**
 * Class creates a Box, which looks like the body of Mario
 * and contains the ability to let Mario jump
 */
function Mario() {
	/** defines jump height */
	var jumpHeight = 200;

	/** defines size of the box (margins Mario image) */
	var size = 50;

	/** creates a box with four parameters
	 *  @param {number} 50 - sets box size
	 *  @param {number} GROUNDLEVEL - initializes height of the ground
	 *  @param {number} size - x value
	 *  @param {number} size - y value
	 */
	this.box = new Box(50, GROUNDLEVEL, size, size);

	/** sets initial velocity to zero */
	var velocityY = 0;

	/** jump function changes velocity and puts out "jump" in console */
	this.jump = function () {
		console.log("jump");
		velocityY = -2;
	}

	/** function draws the final box*/
	this.draw = function () {

		/** changes y value of the box to the current velocity */
		this.box.y = this.box.y + velocityY;

		/** if the boxes current jump height is smaller than the defined jump height the velocity is set to four */
		if (this.box.y < jumpHeight)
			velocityY = 4;

		/** *  if the boxes current jump height is bigger or equal to the ground level the velocity is set to zero*/
		if (this.box.y >= GROUNDLEVEL)
			velocityY = 0;

		/** draws the image of Mario by initializing mario.jpg
		 *  values x and y margin the box and set a fixed size
		 */
		image(images.mario, this.box.left(), this.box.top(), size, size);
	}
}
